const { body } = require('express-validator')
const User = require('../models/user')

exports.registerValidators = [
    body('email')
        .isEmail().withMessage('Введіть коректний email')
        .custom(async (value, { req }) => {
            try {
                const user = await User.findOne({ email: value })
                if (user) {
                    return Promise.reject('Такий email вже заннятий')
                }
            } catch (e) {
                console.log(e)
            }
        })
        .normalizeEmail(),
    body('password', 'Пароль повинен бути мінімум 6 символів')
        .isLength({ min: 6, max: 56 })
        .isAlphanumeric()
        .trim(),
    body('confirm')
        .custom((value, { req }) => {
            if (value !== req.body.password) {
                throw new Error('Паролі повині співпадати')
            }

            return true
        })
        .trim(),
    body('name')
        .isLength({ min: 3 }).withMessage('Імя повиине бути мінімум 3 символа')
        .trim()
];


exports.loginValidators = [
    body('email')
        .isEmail().withMessage('Введіть коректний email')
        .custom(async (value, { req }) => {
            try {
                const user = await User.findOne({ email: value })
                if (user) {
                    return Promise.reject('Такий email вже заннятий')
                }
            } catch (e) {
                console.log(e)
            }
        })
        .normalizeEmail(),
    body('password', 'Пароль повинен бути мінімум 6 символів')
        .isLength({ min: 6, max: 56 })
        .isAlphanumeric()
        .trim(),
];

exports.courseValidators = [
    body('title').isLength({ min: 3 }).withMessage('Мінімальна довжина назви 3 символа').trim(),
    body('price').isNumeric().withMessage('Введіть коректну ціну'),
    body('img', 'Введіть коректний url картинки').isURL()
];