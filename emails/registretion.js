const keys = require('../keys')

module.exports = function (email) {
    return {
        to: email,
        from: keys.EMAIL_FROM,
        subject: 'Аккаунт створений',
        html: `
        <h1>Вітаємо вас в Магазині курсів</h1>
        <p>Ви успішно створили акаунт з email - ${email}</p>
        <span>Навчайтесь в кращих</span>
        <hr/>
        <a href="${keys.BASE_URL}">Магазин курсів</a>
     `
    }
}