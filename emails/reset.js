const keys = require('../keys')

module.exports = function (email, token) {
    return {
        to: email,
        from: keys.EMAIL_FROM,
        subject: 'Відновлення аккаунта',
        html: `
        <h1>Забули пароль?</h1>
        <p>Якщо ні, ігноруйте даний лист</p>
        <p>Якщо так, нажміть на силку нижче</p>
        <p><a href="${keys.BASE_URL}/auth/password/${token}">Відновити доступ</a></p>
        <hr/>
        <a href="${keys.BASE_URL}">Магазин курсів</a>
     `
    }
}